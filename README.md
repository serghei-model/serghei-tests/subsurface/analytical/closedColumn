## Closed column test

This test problem starts with uniform water content profile in a closed 1D soil column. Under gravity, water gradually flows towards the bottom of the column, until hydrostatic pressure profile is reached. 

